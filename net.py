# coding=utf-8
import tensorflow as tf

import config


class NN:
    def __init__(self, session):
        self.session = session

    def train(self, history_batch, previous_weight_batch, price_change_rate_batch):
        weight, loss, pv, _, = self.session.run([cur_weight_op, loss_op, pv_op, train_op], feed_dict={
            history: history_batch,
            previous_weight: previous_weight_batch,
            price_change_rate: price_change_rate_batch,
            batch_size: history_batch.shape[0],
            learning_rate: config.learning_rate})
        return weight, loss, pv

    def trade(self, history_batch, previous_weight_batch, price_change_rate_batch):
        weight, pv = self.session.run([cur_weight_op, pv_op], feed_dict={
            history: history_batch,
            previous_weight: previous_weight_batch,
            price_change_rate: price_change_rate_batch,
            batch_size: history_batch.shape[0],
            learning_rate: config.learning_rate})
        return weight, pv


def init_model():
    # 缩写
    l2 = tf.contrib.layers.l2_regularizer

    # 核心网络
    x = conv2d(history, 3, [1, 2], tf.nn.relu)
    x = conv2d(x, 10, [1, x.shape[2]], tf.nn.relu, l2(scale=5e-9))
    x = tf.concat([x, previous_weight], axis=3)
    x = conv2d(x, 1, [1, 1], None, l2(scale=5e-8))
    x = x[:, :, 0, 0]
    x = tf.concat([tf.zeros([batch_size, 1]), x], axis=1)
    cur_weight = tf.nn.softmax(x)

    # 计算cur_weight经过一个时段以后的值
    cur_weight_final = (price_change_rate * cur_weight) /\
        tf.reduce_sum(price_change_rate * cur_weight, axis=1)[:, None]

    # 切片，将前一个cur_weight_final与后一个cur_weight对齐比较
    w0 = cur_weight_final[:batch_size - 1]
    w1 = cur_weight[1:batch_size]

    # 计算每个时段缴纳手续费后资产剩余率μ
    mu = 1 - tf.reduce_sum(
        tf.abs(w0[:, 1:] - w1[:, 1:]), axis=1)*config.commission_rate

    # 计算收益向量
    pv = tf.reduce_sum(cur_weight * price_change_rate, axis=1) *\
        tf.concat([tf.ones(1), mu], axis=0)

    # 损失op与训练op
    loss = -tf.reduce_mean(tf.log(pv)) + tf.losses.get_regularization_loss()
    train = tf.train.AdamOptimizer(learning_rate).minimize(loss)
    return cur_weight, loss, pv, train


# 缩写
def conv2d(x, filters, kernel_size, activation=None, regularizer=None):
    return tf.layers.conv2d(x, filters, kernel_size, activation=activation, kernel_regularizer=regularizer)


# 需要填入的占位符
history = tf.placeholder(
    tf.float32, [None, config.coin_number, config.window_size, 3], name='history')
previous_weight = tf.placeholder(
    tf.float32, [None, config.coin_number, 1, 1], name='previous_weight')
price_change_rate = tf.placeholder(
    tf.float32, [None, config.coin_number + 1], name='price_change_rate')

# 自动填入的占位符
batch_size = tf.placeholder(tf.int32, name='batch_size')
learning_rate = tf.placeholder(tf.float32, name='learning_rate')

# 导出op
cur_weight_op, loss_op, pv_op, train_op = init_model()
