import argparse
import time

import numpy as np
import tensorflow as tf

import config
import data
import net


class Asset:
    def __init__(self):
        self.name = ""
        self.open = 0
        self.low_history = []
        self.high_history = []
        self.close_history = []


class Agent:
    def __init__(self):
        self.session = tf.Session()
        self.saver = tf.train.Saver()
        self.restore()
        self.nn = net.NN(self.session)
        self.global_step = 0

    def train(self, data_path):
        train_data = data.Data(data_path)
        for echo in range(config.train_echo_max):
            total_loss = 0
            total_pv = 0
            for i in range(train_data.max_batch_index + 1):
                h_batch, pw_batch, pcr_batch = train_data.get_batch(i)
                weight, loss, pv = self.nn.train(h_batch, pw_batch, pcr_batch)
                train_data.rewrite_memory(i, weight)
                total_loss += loss
                total_pv += np.product(pv)
            mean_loss = total_loss / (train_data.max_batch_index + 1)
            mean_pv = total_pv / (train_data.max_batch_index + 1)
            log('echo:', echo, 'loss:', mean_loss)
            log('echo:', echo, 'reward:', mean_pv)
            self.global_step += train_data.max_batch_index + 1
            self.save()

    def trade(self, data_path):
        test_data = data.Data(data_path, 2)
        tatal_pv = 1
        for i in range(test_data.max_batch_index + 1):
            h_batch, pw_batch, pcr_batch = test_data.get_batch(i)
            weight, pv = self.nn.trade(h_batch, pw_batch, pcr_batch)
            test_data.rewrite_memory(i, weight)
            tatal_pv *= pv[1]
        log('reward:', tatal_pv)

    def shutdown(self):
        self.session.close()

    def save(self):
        self.saver.save(self.session, config.chek_path + 'c', self.global_step)

    def restore(self):
        checkpoint = tf.train.latest_checkpoint(config.chek_path)
        if checkpoint:
            log('从检查点恢复', checkpoint)
            self.global_step = int(checkpoint.split('-')[-1])
            self.saver.restore(self.session, checkpoint)
        else:
            log('未找到任何检查点')
            self.global_step = 0
            self.session.run(tf.global_variables_initializer())


def log(*messages):
    print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()),
          ' '.join(str(a) for a in messages))


parser = argparse.ArgumentParser()
parser.add_argument("--train", action="store_true")
parser.add_argument("--trade", action="store_true")
args = parser.parse_args()

a = Agent()
if args.train:
    a.train(config.train_data_path)
elif args.trade:
    a.trade(config.test_data_path)
    a.shutdown()
else:
    a.train(config.train_data_path)
