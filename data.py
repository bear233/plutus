# coding=utf-8
import pickle

import numpy as np

import config


class Data:
    def __init__(self, path, batch_size=config.batch_size):
        coin_number = 0
        period_number = 0
        self.batch_size = batch_size
        with open(path, 'rb') as f:
            raw_data = pickle.load(f)
            coin_number = len(raw_data)
            period_number = len(raw_data[0].low_history)

            # data初始化
            # 所有数字货币的价格信息
            self.data = np.empty((3, coin_number, period_number), np.float32)
            for i, asset in enumerate(raw_data):
                self.data[0, i] = asset.close_history
                self.data[1, i] = asset.high_history
                self.data[2, i] = asset.low_history
            # data.shape (3,m,n) => (3,n,m)
            self.data = np.moveaxis(self.data, 1, -1)

        # vtensors初始化
        # 将data按照windows_size切片并标准化后的结果
        self.max_batch_index = period_number - batch_size - config.window_size
        self.vtensors = np.empty(
            (period_number - config.window_size, 3, config.window_size, coin_number), np.float32)
        for i in range(len(self.vtensors)):
            start = i
            end = start + config.window_size
            self.vtensors[i] = self.data[:, start:end, :]
            self.vtensors[i] = self.vtensors[i] / self.vtensors[i, 0, -1]
        # vtensors.shape (-1,3,n,m) => (-1,m,n,3)
        self.vtensors = np.swapaxes(self.vtensors, 1, -1)

        # wmemory初始化
        # 将神经网络计算出来的weight保存到一个记忆序列中
        self.wmemory = np.zeros(
            (len(self.vtensors) + 1, coin_number), np.float32)

        # prate初始化
        # 每个时段分别的数字货币价格变化率，包括cash
        self.prate = self.data[0, 1:, :] / self.data[0, :-1, :]
        self.prate = self.prate[-len(self.vtensors):]
        ones = np.ones((self.prate.shape[0], 1), np.float32)
        self.prate = np.concatenate((ones, self.prate), axis=1)

    def get_batch(self, index):
        start = index
        end = start + self.batch_size
        return self.vtensors[start:end], self.wmemory[start:end, :, np.newaxis, np.newaxis], self.prate[start:end]

    def rewrite_memory(self, index, new_memory):
        start = index + 1
        end = start + self.batch_size
        self.wmemory[start:end] = new_memory[:, 1:]
