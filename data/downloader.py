# coding=utf-8

import json
import pickle
import time

import requests


# 类声明
class Asset:
    def __init__(self):
        self.name = ""
        self.open = 0
        self.low_history = []
        self.high_history = []
        self.close_history = []


# 常量
START = 1443657600
END = 1538352000
# START = 1542412800
# END = 1546300800
COINS = ['BTS', 'CLAM', 'DASH', 'DOGE', 'ETH',
         'LTC', 'MAID', 'STR', 'XMR', 'XRP']
URL = "https://poloniex.com/public?command=returnChartData&currencyPair=BTC_{}&start={}&end={}&period=1800"

# 变量
assets = []
session = requests.Session()

# 获取所有Asset原始数据
for name in COINS:
    with session.request("get", url=URL.format(name, START, END), timeout=300) as r:
        data = json.loads(r.text)
        asset = Asset()
        asset.name = name.lower()
        asset.open = data[0]['open']
        for period in data:
            asset.low_history.append(period['low'])
            asset.high_history.append(period['high'])
            asset.close_history.append(period['close'])
        assets.append(asset)
        time.sleep(1)
        print(asset.name)

# 序列化对象到文件
with open('train.bin', 'wb') as f:
    pickle.dump(assets, f)
